import { useEffect, useLayoutEffect, useRef, useState } from 'react';

export function useScreenRect() {
  const [rect, setRect] = useState({});
  useLayoutEffect(() => {
    const r = document.body.getBoundingClientRect();
    setRect(r);
  }, []);
  return rect;
}

export function useInterval(callback, delay) {
  const savedCallback = useRef(callback)

  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  useEffect(() => {
    if (!delay && delay !== 0) return;
    const id = setInterval(() => savedCallback.current(), delay)
    return () => clearInterval(id)
  }, [delay])
}
