import React from 'react';
import { Box, Button, Typography } from '@mui/material';
import { Link, useLocation } from 'react-router-dom';

function PageNotFound() {
  const location = useLocation();
  const boxSX = {
    fontStyle: 'italic',
    textAlign: 'center',
    margin: '20px 0 0',
  };
  const message = `You are visiting '${location.pathname}' which is not a correct url`;
  return (
    <Box sx={boxSX}>
      <Typography variant='h2'>Page not found</Typography>
      <Typography varaint='body1'>{message}</Typography>
      <Button variant='text' sx={{ margin: '20px 0 0' }}>
        <Link to='/home'>BACK TO HOME</Link>
      </Button>
    </Box>
  );
}

export default PageNotFound;
