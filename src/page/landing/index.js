import React from 'react';
import { Box, Button, Link, Typography } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';

function AtmLandingPage() {
  const containerSX = {
    height: '100%',
    width: '100%',
    overflow: 'hidden',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
  };
  const linkAreaSX = {
    margin: '40px 0 0',
    display: 'flex',
    flexFlow: {
      xs: 'column nowrap',
      sm: 'row nowrap',
      md: 'row nowrap',
      lg: 'row nowrap',
      xl: 'row nowrap',
    },
    justifyContent: 'space-between',
    alignItems: 'baseline',
    width: 300,
    height: 100,
  };

  return (
    <Box sx={containerSX}>
      <Typography variant='h1'>Auto Match</Typography>
      <Typography variant='subtitle'>An online match recording app for Mario Tennis Aces</Typography>
      <Box sx={linkAreaSX}>
        <Button variant="contained">
          <RouterLink to='/home'>START</RouterLink>
        </Button>
        <Link href='https://gitlab.com'>or contribute codes for us!</Link>
      </Box>
    </Box>
  );
}

export default AtmLandingPage;
