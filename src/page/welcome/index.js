import React from 'react';
import { Box, Typography } from '@mui/material';

function AtmHomePage() {
  return (
    <Box>
      <Typography variant='h1'>Welcome to Auto Match</Typography>
    </Box>
  );
}

export default AtmHomePage;
