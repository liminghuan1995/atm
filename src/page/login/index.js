import React from 'react';
import { Box, Button, Stack, TextField, Typography } from '@mui/material';

import CanvasBackground from './canvas-bg';

function LoginPage() {
  const containerSX = {
    width: 300,
    height: 200,
    boxShadow: 3,
    borderRadius: 4,
    padding: 10
  };

  return (
    <CanvasBackground>
      <Box sx={containerSX}>
        <Stack
          direction='column'
          spacing={2}
          alignItems='center'
          justifyContent='center'
          height='100%'
        >
          <Typography variant='h4'>Log in</Typography>
          <Typography variant='subtitle'>sign in Auto Match</Typography>
          <TextField
            size='small'
            label="Username"
            variant="outlined"
            fullWidth={true}
          />
          <TextField
            size='small'
            label="Password"
            variant="outlined"
            fullWidth={true}
            type='password'
          />
          <Button
            variant='contained'
            size='small'
            fullWidth={true}
          >LOGIN</Button>
        </Stack>
      </Box>
    </CanvasBackground>
  );
}

export default LoginPage;
