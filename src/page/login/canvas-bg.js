import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const CANVAS_NOT_AVAILABLE = 'Your browser doesn\'t support canvas element';
const COLORS = (function() {
  const colorArr = [];
  for (let i = 0; i < 6; i++) {
    for (let j = 0; j < 6; j++) {
      const red = Math.floor(255 - 42.5 * i);
      const green = Math.floor(255 - 42.5 * j);
      const blue = 0;
      colorArr.push({ r: red, g: green, b: blue });
    }
  }
  return colorArr;
}());

function Circle(ctx) {
  this.rect = document.body.getBoundingClientRect();
  this.ctx = ctx;
  this.frame = 0;
  this.finished = false;
  this.init();
}

Circle.prototype.maxR = 40;
Circle.prototype.minR = 20;
Circle.prototype.yTransform = 100;
Circle.prototype.frameCycle = 24 * 6;
Circle.prototype.delta = 1 / (24 * 6);

Circle.prototype.init = function() {
  let r = Math.random() * (this.maxR - this.minR) + this.minR; // minR < r < maxR
  this.r = Math.round(r);
  let x = Math.random() * (this.rect.width - r); // 0 < x < rect.width - r
  this.x = Math.round(x);
  const h = this.rect.height;
  let y = Math.random() * (h - r - (4 / 5) * h) + (4 / 5) * h; // 4/5(rect.height) < y < rect.height - r
  this.y = Math.round(y);
  this.alpha = 0;
  this.scale = 0;
  const colorIdx = Math.round(Math.random() * (COLORS.length - 1));
  this.color = COLORS[colorIdx] // 0 < x < COLORS.length - 1;
};

Circle.prototype.animateByFrame = function() {
  const { r, g, b } = this.color;
  const rgba = `rgba(${r}, ${g}, ${b}, ${this.alpha})`;
  const radius = this.r * this.scale;
  this.ctx.fillStyle = rgba;
  this.ctx.beginPath();
  this.ctx.arc(this.x, this.y, radius, 0, 2 * Math.PI, false);
  this.ctx.fill();
  this.frame++;
  // 0 < -1/2(cos(2πx/t) - 1) < 1, t = frameCycle
  this.alpha = -0.5 * (Math.cos(2 * Math.PI * this.frame / this.frameCycle) - 1);
  this.scale += this.delta;
  this.y -= this.delta * this.yTransform;
  if (this.frame > this.frameCycle) this.finished = true;
};

function CanvasBackground(props) {
  const { children } = props;
  const canvasTag = useRef();
  const containerSX = {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
  };
  const contentSX = {
    zIndex: 2,
  };
  const canvasStyle = {
    width: '100%',
    height: '100%',
    position: 'absolute',
    zIndex: 1,
  };
  const circlesRef = useRef([]);

  useEffect(() => {
    const canvas = canvasTag.current;
    const circles = circlesRef.current;
    const rect = document.body.getBoundingClientRect();
    canvas.width = rect.width;
    canvas.height = rect.height;
    const ctx = canvas.getContext('2d');
    (function animateAll() {
      requestAnimationFrame(() => {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        circles.forEach(c => {
          if (!c.finished) c.animateByFrame();
        });
        animateAll();
      });
    }());
    const interval = setInterval(() => {
      circles.push(new Circle(ctx));
      circlesRef.current = circles.filter(c => !c.finished);
    }, 300);
    return () => clearInterval(interval);
  }, []);

  return (
    <Box sx={containerSX}>
      <canvas ref={canvasTag} style={canvasStyle}>
        {CANVAS_NOT_AVAILABLE}
      </canvas>
      <Box sx={contentSX}>{children}</Box>
    </Box>
  );
}

CanvasBackground.propTypes = {
  children: PropTypes.element.isRequired,
};

export default CanvasBackground;
