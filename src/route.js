import React from 'react';
import { Routes, Route } from 'react-router-dom';

import TopNavLayout from './layout/topnav';
import AtmLandingPage from './page/landing';
import AtmHomePage from './page/welcome';
import PageNotFound from './page/404';
import FullSrcLayout from './layout/fullsrc';
import LoginPage from './page/login';

function AppRoute() {
  return (
    <Routes>
      <Route path='/' element={<AtmLandingPage/>}/>
      <Route element={<TopNavLayout/>}>
        <Route index path='home' element={<AtmHomePage/>}/>
        <Route path='*' element={<PageNotFound/>}/>
      </Route>
      <Route element={<FullSrcLayout/>}>
        <Route path='login' element={<LoginPage/>}/>
      </Route>
    </Routes>
  );
}

export default AppRoute;
