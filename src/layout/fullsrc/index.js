import React from 'react';
import { Outlet } from 'react-router-dom';
import { Box } from '@mui/material';

function FullSrcLayout() {
  const containerSX = {
    height: '100vh',
    width: '100vw',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
  };
  return (
    <Box sx={containerSX}>
      <Outlet/>
    </Box>
  );
}

export default FullSrcLayout;
