import React from 'react';
import { AppBar, Box, Toolbar, Typography } from '@mui/material';
import { Link, Outlet } from 'react-router-dom';

function TopNavLayout() {
  const titleSX = { flexGrow: 1 };
  const contentSX = { padding: '2rem' };

  return (
    <>
      <AppBar position='static'>
        <Toolbar>
          <Typography variant="h6" component="h1" sx={titleSX}>
            <Link to='/home'>Auto Match</Link>
          </Typography>
        </Toolbar>
      </AppBar>
      <Box sx={contentSX}>
        <Outlet/>
      </Box>
    </>
  );
}

export default TopNavLayout;
