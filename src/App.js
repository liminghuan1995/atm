import React from 'react';

import AppRoute from './route';

function App() {
  return (
    <AppRoute/>
  );
}

export default App;
